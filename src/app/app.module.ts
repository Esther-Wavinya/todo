import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DragDropModule } from "@angular/cdk/drag-drop";
import { AppRoutingModule } from './app-routing.module';
import { AllComponent } from './all/all.component';
import { ActiveComponent } from './active/active.component';
import { CompletedComponent } from './completed/completed.component';
import { ClearcompletedComponent } from './clearcompleted/clearcompleted.component';






const routes: Routes = [
  { path: 'all', component: AllComponent },
  { path: 'active', component: ActiveComponent},
  { path: 'completed', component: CompletedComponent },
  { path: 'clearcompleted', component: ClearcompletedComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    TodoListComponent,
    AllComponent,
    ActiveComponent,
    CompletedComponent,
    ClearcompletedComponent
  ],
  imports: [BrowserModule, RouterModule.forRoot(routes), FormsModule, ReactiveFormsModule, DragDropModule, AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
