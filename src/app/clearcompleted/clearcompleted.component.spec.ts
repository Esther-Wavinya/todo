import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClearcompletedComponent } from './clearcompleted.component';

describe('ClearcompletedComponent', () => {
  let component: ClearcompletedComponent;
  let fixture: ComponentFixture<ClearcompletedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClearcompletedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClearcompletedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
